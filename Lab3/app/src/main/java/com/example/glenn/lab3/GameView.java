package com.example.glenn.lab3;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

/**
 * Created by glenn on 18.03.2018.
 */

public class GameView extends View {
    private Ball ball;

    private int width;
    private int height;
    private int backgroundPadding;
    private Paint backgroundPaint;

    public GameView(Context context) {
        super(context);

        Paint ballPaint = new Paint();
        ballPaint.setColor(Color.RED);

        ball = new Ball(ballPaint);

        backgroundPaint = new Paint();
        backgroundPaint.setColor(Color.BLACK);
        backgroundPadding = 10;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        ball.x = w / 2;
        ball.y = h / 2;
        ball.radius = w / 20;

        width = w;
        height = h;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawRect(backgroundPadding, backgroundPadding, width - backgroundPadding, height - backgroundPadding, backgroundPaint);
        canvas.drawCircle(ball.x, ball.y, ball.radius, ball.paint);

        invalidate();
    }

    public static final double ACCELERATION_MODIFIER = -1.000;
    public void updatePos(float[] orientation)
    {
        ball.speedX += Math.toDegrees(orientation[1]);
        ball.speedY += Math.toDegrees(orientation[2]);

        ball.updateSpeed(Math.toDegrees(orientation[1]) * ACCELERATION_MODIFIER, Math.toDegrees(orientation[2]) * ACCELERATION_MODIFIER);

        ball.updatePos();

        checkCollision();
    }

    private boolean hasCollided = false;
    public void checkCollision()
    {
        int collisions = 0;

        if(ball.x - ball.radius < backgroundPadding)
        {
            ball.x = backgroundPadding + ball.radius;

            collisions++;
            callback();
        }
        else if(ball.x + ball.radius > width - backgroundPadding)
        {
            ball.x = width - backgroundPadding - ball.radius;

            collisions++;
            callback();
        }

        if(ball.y - ball.radius < backgroundPadding)
        {
            ball.y = backgroundPadding + ball.radius;

            collisions++;
            callback();
        }
        else if(ball.y + ball.radius > height - backgroundPadding)
        {
            ball.y = height - backgroundPadding - ball.radius;

            collisions++;
            callback();
        }

        if(collisions == 0)
        {
            hasCollided = false;
        }
    }

    private void callback()
    {
        if(!hasCollided)
        {
            ((MainActivity)getContext()).collisionEvent();
            hasCollided = true;

        }
    }
}
