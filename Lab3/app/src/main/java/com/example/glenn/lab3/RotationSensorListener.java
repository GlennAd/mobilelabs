package com.example.glenn.lab3;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.renderscript.Matrix4f;
import android.util.Log;

/**
 * Created by glenn on 18.03.2018.
 */

public class RotationSensorListener implements SensorEventListener {

    private Matrix4f rotationMatrix;
    private GameView view;

    RotationSensorListener(GameView view)
    {
        super();

        this.view = view;
        rotationMatrix = new Matrix4f();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() != Sensor.TYPE_ROTATION_VECTOR)
        {
            return;
        }

        SensorManager.getRotationMatrixFromVector(rotationMatrix.getArray(), event.values);

        float[] orientation = new float[3];

        SensorManager.getOrientation(rotationMatrix.getArray(), orientation);

        view.updatePos(orientation);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
