package com.example.glenn.lab3;

import android.graphics.Paint;
import android.graphics.Point;

/**
 * Created by glenn on 18.03.2018.
 */

public class Ball {
    public int x;
    public int y;
    public double speedX = 0.0f;
    public double speedY = 0.0f;

    public int radius;
    public Paint paint;

    public Ball(Paint paint)
    {
        this.paint = paint;
    }

    public void updateSpeed(double  x, double y)
    {
        speedX = x;
        speedY = y;
    }

    public void updatePos() {
        x += (int) speedX;
        y += (int) speedY;
    }
}
