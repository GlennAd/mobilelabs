package com.example.glenn.lab3;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity {
    private SensorManager sensorManager;
    private Sensor rotationSensor;
    private RotationSensorListener eventListener;
    private Vibrator vibrator;
    private ToneGenerator toneGenerator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

        GameView view = new GameView(this);

        eventListener = new RotationSensorListener(view);

        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        toneGenerator = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);

        setContentView(view);
    }

    @Override
    protected void onResume() {
        sensorManager.registerListener(eventListener, rotationSensor, 16);
        super.onResume();
    }

    @Override
    protected void onPause() {
        sensorManager.unregisterListener(eventListener, rotationSensor);
        super.onPause();
    }

    public void collisionEvent()
    {
        toneGenerator.startTone(ToneGenerator.TONE_PROP_BEEP);
        if(vibrator != null)
        {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                vibrator.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
            }
            else
            {
                vibrator.vibrate(100);
            }
        }
    }
}
