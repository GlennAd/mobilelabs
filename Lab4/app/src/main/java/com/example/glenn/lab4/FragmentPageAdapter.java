package com.example.glenn.lab4;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;

public class FragmentPageAdapter extends FragmentPagerAdapter {
    private Context context;


    public FragmentPageAdapter(FragmentManager manager, Context context)
    {
        super(manager);
        this.context = context;
    }




    @Override
    public CharSequence getPageTitle(int position) {
        SpannableStringBuilder sb;

        switch (position)
        {
            case 0:
                Drawable drawable = context.getResources().getDrawable(R.drawable.common_google_signin_btn_text_dark);
                drawable.setBounds(0, 0, 48, 48);
                ImageSpan imageSpan = new ImageSpan(drawable);

                sb = new SpannableStringBuilder("      messages");
                sb.setSpan(imageSpan, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                break;

            case 1:
                drawable = context.getResources().getDrawable(R.drawable.ic_launcher_background);
                drawable.setBounds(0, 0, 48, 48);
                imageSpan = new ImageSpan(drawable);

                sb = new SpannableStringBuilder("   users");
                sb.setSpan(imageSpan, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                break;

                default:
                    sb = new SpannableStringBuilder("ERROR");
        }

        return sb;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new MessagesFragment();

            case 1:
                return new UserList();

        }


        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
