package com.example.glenn.lab4;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.Tag;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewParent;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseDatabase db;

    private static final int MAX_USERNAME_LENGTH = 35;
    private String userName;

    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance();

        ViewPager viewPager = findViewById(R.id.viewpager);

        FragmentPagerAdapter adapter = new FragmentPageAdapter(getSupportFragmentManager(), this);

        viewPager.setAdapter(adapter);

        TabLayout tabLayout = findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        Login();
    }

    protected String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }


    private void Login(){
        final SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);

        userName = pref.getString("username", null);

        if(userName == null)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Please choose a nickname");

            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            input.setText(getSaltString());
            builder.setView(input);

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String name = input.getText().toString();

                    if(name.isEmpty()){
                        Toast.makeText(MainActivity.this, "Input cannot be empty.", Toast.LENGTH_LONG).show();
                        Login();
                    }
                    else if(name.length() > MAX_USERNAME_LENGTH)
                    {
                        Toast.makeText(MainActivity.this, "Name must be less then " + MAX_USERNAME_LENGTH + " long", Toast.LENGTH_SHORT).show();
                        Login();
                    }
                    else
                    {
                        userName = name;

                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("username", userName);
                        editor.apply();

                        DatabaseReference myRef = db.getReference("users");
                        DatabaseReference newUser = myRef.push();

                        Log.e("TAG", "Just checking");

                        newUser.setValue(userName);
                    }
                }
            });

            builder.show();
        }
    }


    @Override
    protected void onStart()
    {
        super.onStart();

        if(pendingIntent != null)
        {
            AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pendingIntent);
        }

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if(currentUser == null)
        {
            mAuth.signInAnonymously()
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful())
                            {
                            }
                            else
                            {
                            }
                        }
                    });
        }
        else
        {
            Log.e("TAG", "Already signed in");
        }
    }


    @Override
    protected void onStop()
    {
        super.onStop();


        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String timeStored = preferences.getString("refreshRate", "10");
        int timeStoredInMillis = Integer.parseInt(timeStored) * 60 * 1000;

        Intent intent = new Intent(this, NotificationService.class);
        pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, intent, 0);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), timeStoredInMillis, pendingIntent);

        startService(intent);
    }

    public void startSettings(View view)
    {
        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        startActivity(intent);
    }
}
