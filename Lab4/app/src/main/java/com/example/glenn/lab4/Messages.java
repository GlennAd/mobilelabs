package com.example.glenn.lab4;

public class Messages {
    private String date;
    private String user;
    private String message;

    public Messages(String date, String user, String message)
    {
        this.date = date;
        this.user = user;
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}