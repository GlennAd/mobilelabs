package com.example.glenn.lab4;

import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class SpecifiedUserList extends AppCompatActivity {
    private static final String TAG = "SPECIFIED_USED_LIST";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_listview);

        FirebaseDatabase db = FirebaseDatabase.getInstance();
        ListView messages = findViewById(R.id.listView);
        final ArrayList<Messages> list = new ArrayList<>();
        final MessageAdapter adapter = new MessageAdapter(this, list);


        final DatabaseReference ref = db.getReference("messages");

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot messageSnapshot: dataSnapshot.getChildren()) {
                    String date = "null";
                    String user = "null";
                    String message = "null";

                    for(DataSnapshot child : messageSnapshot.getChildren())
                    {
                        Log.d(TAG, "Number of children: " +  messageSnapshot.getChildrenCount());
                        if(child.getKey().equals("d"))
                        {
                            date = (String)child.getValue();
                        }
                        else if(child.getKey().equals("u"))
                        {
                            user = (String)child.getValue();
                        }
                        else if(child.getKey().equals("m"))
                        {
                            message = (String)child.getValue();
                        }
                        else
                        {
                            Log.e(TAG, "THIS SHOULDN'T HAPPEN");
                        }
                    }

                    if(user.equals(getIntent().getStringExtra("username")))
                    {
                        list.add(new Messages(date, user, message));
                    }
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());

            }
        };
        ref.addValueEventListener(postListener);

        messages.setAdapter(adapter);

    }
}
