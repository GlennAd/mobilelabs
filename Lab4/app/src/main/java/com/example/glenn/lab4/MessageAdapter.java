package com.example.glenn.lab4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class MessageAdapter extends BaseAdapter {
    private ArrayList<Messages> messages;
    private LayoutInflater inflater;

    public MessageAdapter(Context context, ArrayList<Messages> messages)
    {
        super();
        this.inflater = LayoutInflater.from(context);
        this.messages = messages;
    }

    @Override
    public boolean isEmpty() {
        return messages.isEmpty();
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Messages getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.message_layout, parent, false);

        TextView message = convertView.findViewById(R.id.messageText);
        TextView username = convertView.findViewById(R.id.usernameText);

        username.setText(messages.get(position).getUser());
        message.setText(messages.get(position).getMessage());


        return convertView;
    }
}
