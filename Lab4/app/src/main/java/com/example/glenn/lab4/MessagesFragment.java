package com.example.glenn.lab4;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessagesFragment extends Fragment {
    ArrayList<Messages> messages;
    MessageAdapter adapter;

    private static final String TAG = "MESSAGE_FRAGMENT";
    private static final int MAX_MESSAGE_LENGTH = 256;
    public MessagesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.meesage_list_layout, container, false);

        messages = new ArrayList<>();

        FirebaseDatabase db = FirebaseDatabase.getInstance();

        final DatabaseReference ref = db.getReference("messages");

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                messages.clear();
                for (DataSnapshot messageSnapshot: dataSnapshot.getChildren()) {
                    String date = "null";
                    String user = "null";
                    String message = "null";

                    for(DataSnapshot child : messageSnapshot.getChildren())
                    {
                        Log.d(TAG, "Number of children: " +  messageSnapshot.getChildrenCount());
                        if(child.getKey().equals("d"))
                        {
                            date = (String)child.getValue();
                        }
                        else if(child.getKey().equals("u"))
                        {
                            user = (String)child.getValue();
                        }
                        else if(child.getKey().equals("m"))
                        {
                            message = (String)child.getValue();
                        }
                        else
                        {
                            Log.e(TAG, "THIS SHOULDN'T HAPPEN");
                        }
                    }

                    messages.add(new Messages(date, user, message));
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());

            }
        };
        ref.addValueEventListener(postListener);

        adapter = new MessageAdapter(getActivity(), messages);
        ListView view = rootView.findViewById(R.id.listviewMessageList);
        view.setAdapter(adapter);

        Button sendButton = rootView.findViewById(R.id.sendButton);



        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText text = rootView.findViewById(R.id.messageInput);

                String message = text.getText().toString();

                if(!message.isEmpty() && message.length() <= MAX_MESSAGE_LENGTH)
                {
                    DatabaseReference newMessage = ref.push();
                    DatabaseReference child = newMessage.child("d");

                    Date currentDate=new Date(System.currentTimeMillis());
                    SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    String date = sfd.format(currentDate);

                    child.setValue(date);

                    child = newMessage.child("u");

                    final SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
                    child.setValue(pref.getString("username", "USERNAME_NOT_FOUND"));

                    child = newMessage.child("m");

                    child.setValue(message);
                }
            }
        });

        return rootView;
    }



}
