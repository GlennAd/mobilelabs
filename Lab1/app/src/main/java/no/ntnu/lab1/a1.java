package no.ntnu.lab1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class a1 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);

        long selectedItem = sharedPref.getInt("Selector", 0);

        setContentView(R.layout.activity_a1);

        Spinner dropDown = findViewById(R.id.L1);

        String[] items = new String[]{"option1", "option2", "option3", "hello", "world"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);

        dropDown.setAdapter(adapter);

        dropDown.setSelection((int)selectedItem);
    }

    public void switchTOA2(View v)
    {
        Intent startA2 = new Intent(this, a2.class);
        EditText text = findViewById(R.id.T1);
        startA2.putExtra("Text", text.getText().toString());
        startActivity(startA2);
    }


    @Override
    protected void onStop() {
        super.onStop();

        SharedPreferences sharePref = getPreferences(Context.MODE_PRIVATE);

        Spinner dropDown = findViewById(R.id.L1);

        SharedPreferences.Editor editor = sharePref.edit();
        editor.putInt("Selector", dropDown.getSelectedItemPosition());

        editor.commit();
    }
}
