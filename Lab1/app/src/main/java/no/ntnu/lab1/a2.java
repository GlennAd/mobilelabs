package no.ntnu.lab1;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class a2 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2);

        TextView textField = findViewById(R.id.T2);
        textField.setText("Hello " + getIntent().getStringExtra("Text"));
    }

    public void switchToA3(View v)
    {
        Intent startA3 = new Intent(this, a3.class);
        startActivityForResult(startA3, 1);
    }

    @Override
    protected  void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == 1)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                TextView textField = findViewById(R.id.T3);

                textField.setText("From A3: " + data.getStringExtra("Text"));
            }
        }
    }
}
