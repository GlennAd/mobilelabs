package no.ntnu.lab1;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.EditText;

public class a3 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3);
    }

    public void switchToA2(View v)
    {
        Intent intent = new Intent();
        EditText textField = findViewById(R.id.T4);
        intent.putExtra("Text", textField.getText().toString());
        setResult(Activity.RESULT_OK, intent);

        finish();
    }
}