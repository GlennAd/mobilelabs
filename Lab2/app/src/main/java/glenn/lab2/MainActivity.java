package glenn.lab2;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private ArrayList<RSSItem> RSSFeed = new ArrayList<>();
    private static final String TAG = "MainActivity";

    private String feedTitle;
    private String feedLink;
    private String feedDescription;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("RSS 2.0 reader");

        Timer timer = new Timer();

        TimerTask task = new TimerTask()
        {
            @Override
            public void run()
            {
                new FetchFeedTask().execute();
            }
        };

        int minutes = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("refreshRate", 10);
        timer.schedule(task, 0, 1000 * 60 * minutes);
    }


    public void getRSSFeed(View view)
    {
        new FetchFeedTask().execute();
    }

    public void startSettings(View view)
    {
        Intent intent = new Intent(this, SettingActivity.class);

        startActivityForResult(intent, 0);
    }

    public void startWebPage(String url)
    {
        Intent intent = new Intent(this, WebPage.class);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                new FetchFeedTask().execute();
            }
        }
    }

    public ArrayList<RSSItem> parseFeed(InputStream inputStream) throws XmlPullParserException, IOException
    {
        String title = null;
        String description = null;
        String link = null;
        boolean isItem = false;

        ArrayList<RSSItem> rssItems = new ArrayList<RSSItem>();

        try
        {
            XmlPullParser xmlParser = Xml.newPullParser();
            xmlParser.setInput(inputStream, null);

            xmlParser.nextTag();

            while(xmlParser.next() != XmlPullParser.END_DOCUMENT)
            {
                int eventType = xmlParser.getEventType();

                String name = xmlParser.getName();


                if(name == null)
                {
                    continue;
                }

                if(eventType == XmlPullParser.END_TAG)
                {
                    if(name.equalsIgnoreCase("Item")){
                        isItem = false;
                    }
                    continue;
                }

                if (eventType == XmlPullParser.START_TAG) {
                    if (name.equalsIgnoreCase("item")) {
                        isItem = true;
                        title = null;
                        link = null;
                        description = null;
                        continue;
                    }
                }

                String result = "";
                if (xmlParser.next() == XmlPullParser.TEXT) {
                    result = xmlParser.getText();
                    xmlParser.nextTag();
                }

                if (name.equalsIgnoreCase("title")) {
                    title = result;
                } else if (name.equalsIgnoreCase("link")) {
                    link = result;
                } else if (name.equalsIgnoreCase("description")) {
                    description = result;
                }

                if (title != null && link != null && description != null) {
                    if(isItem) {
                        RSSItem item = new RSSItem(title, description, link);
                        rssItems.add(item);
                    }
                    else {
                        feedTitle = title;
                        feedLink = link;
                        feedDescription = description;
                    }

                    title = null;
                    link = null;
                    description = null;
                    isItem = false;

                    if(rssItems.size() > PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getInt("feedLimit", 10))
                    {
                        break;
                    }
                }

            }


        }
        finally
        {
            inputStream.close();
        }

        return rssItems;
    }

    public void updateFeed()
    {
        ListView feed = findViewById(R.id.RssFeed);

        CustomAdapter adapter = new CustomAdapter(this, RSSFeed);

        feed.setAdapter(adapter);
        feed.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String url = ((RSSItem)adapterView.getItemAtPosition(i)).getLink();
                startWebPage(url);
            }
        });
    }

    private class FetchFeedTask extends AsyncTask<Void, Void, Boolean>
    {
        private String link;


        @Override
        protected Boolean doInBackground(Void... voids) {
            try
            {
                if(!link.startsWith("http://") && !link.startsWith("https://")) {
                    link = "http://" + link;
                }

                URL url = new URL(link);

                InputStream inputStream = url.openConnection().getInputStream();

                RSSFeed = parseFeed(inputStream);
                return true;
            }
            catch(MalformedURLException exception)
            {
                Log.e(TAG, "Input URL is malformed.\n", exception);
            }
            catch(IOException exception)
            {
                Log.e(TAG, "Error getting input stream.\n", exception);
            }
            catch(XmlPullParserException exception)
            {
                Log.e(TAG, "Error parsing RSS feed.\n", exception);
            }

            return false;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            link = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("url", "https://www.vg.no/rss/feed/");
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);

            if(success)
            {
                updateFeed();
            }
        }
    }
}
