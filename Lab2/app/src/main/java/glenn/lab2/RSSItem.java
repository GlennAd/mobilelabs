package glenn.lab2;

/**
 * Created by glenn on 12.03.2018.
 */

public class RSSItem {
    private String title;
    private String description;
    private String link;

    public  RSSItem(String title, String description, String link)
    {
        this.title = title;
        this.description = description;
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }
}
