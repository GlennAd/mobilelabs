package glenn.lab2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class SettingActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
    }


    public void ApplyChanges(View view)
    {
        EditText URLTextField = findViewById(R.id.url);
        Spinner feedLimit = findViewById(R.id.feedLimitSpinner);
        Spinner refreshRate = findViewById(R.id.refreshRateSpinner);

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());


        SharedPreferences.Editor editor = pref.edit();

        editor.putString("url", URLTextField.getText().toString());
        int number = Integer.parseInt(feedLimit.getSelectedItem().toString());
        editor.putInt("feedLimit", number);

        int[] refreshRates = {10, 60, 1440};

        editor.putInt("refreshRate", refreshRates[refreshRate.getSelectedItemPosition()]);

        editor.commit();

        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }
}
