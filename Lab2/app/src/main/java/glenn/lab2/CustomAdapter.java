package glenn.lab2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Glenn Adrian Nordhus on 15/03/2018.
 */

public class CustomAdapter extends ArrayAdapter<RSSItem> {
    private ArrayList<RSSItem> feed = new ArrayList<>();

    public CustomAdapter(Context context, ArrayList<RSSItem> feed)
    {
        super(context, R.layout.rss_item_layout, feed);
        this.feed = feed;
    }

    @Override
    public int getCount() {
        return feed.size();
    }

    @Override
    public RSSItem getItem(int i) {
        return feed.get(i);
    }

    @Override
    public long getItemId(int i) {

        return i;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup){
        ViewData viewData = new ViewData();

        if(view == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.rss_item_layout, viewGroup, false);

            viewData.title = view.findViewById(R.id.itemTitle);
            viewData.description = view.findViewById(R.id.itemDesc);

            view.setTag(viewData);
        } else {
            viewData = (ViewData) view.getTag();
        }

        viewData.title.setText(feed.get(pos).getTitle());
        viewData.description.setText(feed.get(pos).getDescription());
        viewData.position = pos;

        return view;

    }

    private class ViewData
    {
        TextView title;
        TextView description;
        int position;
    }
}
